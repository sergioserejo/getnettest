package runners;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/test/resources/features/PesquisarItem.feature",
        glue = "steps",
        plugin = {"pretty","html:target/report-html","json:target/report.json","de.monochromata.cucumber.report.PrettyReports:target/cucumber"},
        monochrome = false,
        snippets = CucumberOptions.SnippetType.CAMELCASE,
        dryRun = false
)
public class PesquisarItemRunner {
}
