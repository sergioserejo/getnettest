package steps;

import core.DriverFactory;
import io.cucumber.java.After;
import io.cucumber.java.AfterStep;
import io.cucumber.java.Scenario;
import org.apache.commons.io.FileUtils;
import org.junit.AfterClass;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import java.io.File;
import java.io.IOException;
import java.util.Date;

import static core.DriverFactory.killDriver;

public class Hooks {

    @AfterStep
    public void screenShot (Scenario scenario){
        if (scenario.isFailed() == true) {
            try {
                File imagem = ((TakesScreenshot) DriverFactory.getDriver()).getScreenshotAs(OutputType.FILE);
                FileUtils.copyFile(imagem, new File("target/screenshots/" + scenario.getName() + new Date() + ".png"));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @After
    public void thearDown (){
        killDriver();
    }

}
