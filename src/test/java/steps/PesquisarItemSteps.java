package steps;

import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.Entao;
import io.cucumber.java.pt.Quando;
import pages.HomePage;
import pages.SearchResultPage;

import static core.DriverFactory.getDriver;
import static org.junit.Assert.assertEquals;

public class PesquisarItemSteps {


    HomePage homePage = new HomePage();
    SearchResultPage searchResultPage = new SearchResultPage();

    @Dado("que o usuário acessa o site {string}")
    public void que_o_usuário_acessa_o_site(String url) {
        homePage.accessSystem(url);
    }

    @Quando("pesquisar por {string}")
    public void pesquisar_por(String itemSearch) {
        homePage.searchItem(itemSearch);
    }

    @Quando("seleciona no resultado {string}")
    public void seleciona_no_resultado(String textSearchResult) {
        searchResultPage.clickItemSearch(textSearchResult);
    }

    @Entao("é mostrado um modal com titulo {string}")
    public void é_mostrado_um_modal_com_titulo(String textTitleModal) {
        assertEquals(searchResultPage.getTitleModal(),textTitleModal);
    }

}
