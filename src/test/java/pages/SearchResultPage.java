package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import static core.DriverFactory.getDriver;

public class SearchResultPage extends BasePage {

    public void clickItemSearch(String text){
        WebElement itemSearch = waitElementVisible(getDriver().findElement(By.xpath("//h3[text()='"+text+"']")));
        itemSearch.click();
    }

    public String getTitleModal(){
        WebElement modal = waitElementVisible(getDriver().findElement(By.xpath("//div[@data-modal='10783']/div[@class='o-modal__title']")));
        return modal.getText();
    }
}
