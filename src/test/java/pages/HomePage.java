package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

import java.util.concurrent.TimeUnit;

import static core.DriverFactory.getDriver;

public class HomePage extends BasePage {

    public void clickButtonSearch(){
        waitForLoad(getDriver());
        WebElement search = waitElementVisible(getDriver().findElement(By.id("search-trigger")));
        search.click();
    }

    public void setTextSearch( String text){
        WebElement inputSearch = waitElementVisible(getDriver().findElement(By.id("global-search-input")));
        inputSearch.sendKeys(text);
    }

    public void clickButtonSubmit(){
        WebElement buttonSubmit = waitElementVisible(getDriver().findElement(By.xpath("//button[@type='submit']")));
        buttonSubmit.click();
    }

    public void accessSystem ( String url){
        getDriver().get(url);
    }

    public void searchItem(String textSerch){
        clickButtonSearch();
        setTextSearch(textSerch);
        clickButtonSubmit();
    }
}
