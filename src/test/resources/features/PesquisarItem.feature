#language: pt

  Funcionalidade: Pesquisar item

    Cenário: Pesquisa de item no site
      Dado que o usuário acessa o site "https://site.getnet.com.br/"
      Quando pesquisar por "superget"
      E seleciona no resultado "Onde posso realizar saques com o meu Cartão SuperGet?"
      Entao é mostrado um modal com titulo "Onde posso realizar saques com o meu Cartão SuperGet?"
