# Tecnologia

Foram usados os seguindos itens:
- Cucumber
- Selenium
- Java
- WebDriverManager

# Por que usar WebDriverManager

Para usar o Selenium WebDriver é necessário baixar o chamado driver, ou seja, um arquivo binário que permita ao WebDriver navegadores.
Isso é bastante irritante, pois obriga a vincular diretamente esse driver ao seu código-fonte. O WebDriverManager vem em socorro, executando de maneira automatizada todo isso.

# CI/CD
No projeto foi adicionado CI/CD onde temos dois stages ( build e test ), para executar o pipeline [clique aqui](https://gitlab.com/sergioserejo/getnettest/-/pipelines)  e selecione Run Pipeline.